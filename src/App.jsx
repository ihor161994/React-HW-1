import Header from './components/Header/Header';
import './App.css';
import Courses from './components/Courses/Courses';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import CreateCourse from './components/CreateCourse/CreateCourse';
import { mockedCoursesList, mockedAuthorsList } from './datas/data';

import { useState } from 'react';

function App() {
	const [authorsList, setAuthorsList] = useState(mockedAuthorsList);
	const [coursesList, setCoursesList] = useState(mockedCoursesList);

	return (
		<div className='App'>
			<Header />
			<Router>
				<Routes>
					<Route
						exact
						path='/'
						element={
							<Courses authorsList={authorsList} coursesList={coursesList} />
						}
					/>
					<Route
						exact
						path='/createCourse'
						element={
							<CreateCourse
								authorsList={authorsList}
								setAuthorsList={setAuthorsList}
								setCoursesList={setCoursesList}
							/>
						}
					/>
					<Route path='*' element={<Courses />} />
				</Routes>
			</Router>
		</div>
	);
}

export default App;
