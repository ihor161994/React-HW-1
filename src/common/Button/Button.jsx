import React from 'react';
import styles from './Button.module.scss';
export const Button = ({ text, onClick }) => (
	<button className={styles.button} onClick={onClick}>
		{text}
	</button>
);
