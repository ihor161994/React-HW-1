import React from 'react';
import styles from './Input.module.scss';

const Input = ({ placeholderText, labelText, onChange, ...props }) => {
	return (
		<div className={styles.container}>
			<label htmlFor='html'>{labelText}</label>
			<input
				type='text'
				id='html'
				placeholder={placeholderText}
				onChange={onChange}
				{...props}
			/>
		</div>
	);
};

export default Input;
