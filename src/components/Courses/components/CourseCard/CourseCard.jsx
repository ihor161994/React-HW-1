import React from 'react';
import styles from './CourseCard.module.scss';
import { Button } from '../../../../common/Button/Button';
import {
	pipeDurationDate,
	pipeDurationTime,
} from '../../../../helpers/pipeDuration';
const CourseCard = ({
	data: { title, description, creationDate, duration },
	authors,
}) => {
	return (
		<div className={styles.card}>
			<div className={styles.contentContainer}>
				<h3>{title}</h3>
				<p>{description}</p>
			</div>
			<div className={styles.infoContainer}>
				<p>
					Authors:<span>{authors}</span>
				</p>
				<p>
					Duration:<span>{pipeDurationTime(duration)}</span>
				</p>
				<p>
					Created:<span>{pipeDurationDate(creationDate)}</span>
				</p>
				<Button text={'Show course'} />
			</div>
		</div>
	);
};

export default CourseCard;
