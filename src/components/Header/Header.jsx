import React from 'react';
import styles from './Header.module.scss';
import { Logo } from './components/Logo/Logo';
import { Button } from '../../common/Button/Button';

const Header = () => {
	return (
		<div className='container'>
			<div className={styles.header}>
				<Logo />
				<div className={styles.tools}>
					<div className={styles.userName}>Dave</div>
					<Button text={'Logout'} />
				</div>
			</div>
		</div>
	);
};

export default Header;
